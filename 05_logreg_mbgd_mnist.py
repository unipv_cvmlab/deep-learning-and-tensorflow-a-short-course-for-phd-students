""" Simple logistic regression model for MNIST
with tf.data module
MNIST dataset: yann.lecun.com/exdb/mnist/
Created by Chip Huyen (chiphuyen@cs.stanford.edu)
CS20: "TensorFlow for Deep Learning Research"
cs20.stanford.edu
Lecture 03
"""

import os
import time
import warnings

import numpy as np
import tensorflow as tf

import utils

"""
Adapted by marco.piastra@unipv.it, gianluca.gerard01@universitadipavia.it,
           andrea.pedrini@unipv.it, mirto.musci@unipv.it
"""

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
warnings.simplefilter("ignore")

# for deterministic repeatability
np.random.seed(1)
tf.set_random_seed(2)

# model parameters
learning_rate = 0.01
batch_size = 128
n_epochs = 30

img_size = 28 * 28
n_classes = 10

# read in data
mnist_folder = 'data/mnist'
utils.download_mnist(mnist_folder)
train, _, test = utils.read_mnist(mnist_folder, flatten=True)

img = tf.placeholder(dtype=tf.float32, shape=(batch_size, img_size))
label = tf.placeholder(dtype=tf.int32, shape=(batch_size, n_classes))

# create weights and bias
# W is initialized to random variables with mean of 0, stddev of 0.01
# b is initialized to 0
# shape of W depends on the dimension of X and Y so that Y = tf.matmul(X, W)
# shape of b depends on Y
W = tf.get_variable('weights', (img_size, n_classes),  initializer=tf.random_normal_initializer(0, 0.01))
b = tf.get_variable('biases', (n_classes,), initializer=tf.zeros_initializer())

# build model
# the model that returns the logits.
# this logits will be later passed through softmax layer
logits = tf.matmul(img, W) + b

# define loss function
# use cross entropy of softmax of logits as the loss function
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=label, logits=logits))

# define optimizer
# using Adam Optimizer with pre-defined learning rate to minimize loss
optimizer = tf.train.AdamOptimizer().minimize(loss)

# calculate accuracy with test set
preds = tf.nn.softmax(logits)
correct_predictions = tf.equal(tf.argmax(preds, 1), tf.argmax(label, 1))
accuracy = tf.reduce_sum(tf.cast(correct_predictions, tf.float32))

writer = tf.summary.FileWriter('./graphs/logreg_mgbd_mnist', tf.get_default_graph())
with tf.Session() as sess:
    start_time = time.time()
    sess.run(tf.global_variables_initializer())

    train_img = train[0]
    train_label = train[1]

    test_img = test[0]
    test_label = test[1]

    n_train_batches = int(train_img.shape[0] / batch_size)
    n_test_batches = int(test_img.shape[0] / batch_size)

    # train the model n_epochs times
    for epoch in range(n_epochs):
        total_loss = 0
        for j in range(n_train_batches):
            start = j * batch_size
            end = start + batch_size
            _, l = sess.run([optimizer, loss], feed_dict={img: train_img[start:end],
                                                          label: train_label[start:end]})
            total_loss += l

        print('Epoch {0}: average loss {1}'.format(epoch, total_loss / n_train_batches))

        # test the model for accuracy
        test_accuracy = 0
        for j in range(n_test_batches):
            start = j * batch_size
            end = start + batch_size
            accuracy_batch = sess.run(accuracy, feed_dict={img: test_img[start:end],
                                                           label: test_label[start:end]})
            test_accuracy += accuracy_batch

        print('    Test accuracy {0}'.format(test_accuracy / (n_test_batches * batch_size)))

    print('Total time: {0} seconds'.format(time.time() - start_time))

writer.close()
