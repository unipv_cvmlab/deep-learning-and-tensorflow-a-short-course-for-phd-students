#!/bin/bash

mkdir -p data

FILE=download/kagglecatsanddogs_3367a.zip
if [ ! -f $FILE ]; then
  wget --directory-prefix=download/ https://download.microsoft.com/download/3/E/1/3E1C3F21-ECDB-4869-8368-6DEBA77B919F/kagglecatsanddogs_3367a.zip
  unzip $FILE -d download/
fi

mkdir -p data/dogs_and_cats/train/cats
mkdir -p data/dogs_and_cats/train/dogs
mkdir -p data/dogs_and_cats/test/cats
mkdir -p data/dogs_and_cats/test/dogs

for ((i=0;i<=999;i++));
do
   cp download/PetImages/Cat/$i.jpg data/dogs_and_cats/train/cats/
   cp download/PetImages/Dog/$i.jpg data/dogs_and_cats/train/dogs/
done

for ((i=1000;i<=1400;i++));
do
   cp download/PetImages/Cat/$i.jpg data/dogs_and_cats/test/cats/
   cp download/PetImages/Dog/$i.jpg data/dogs_and_cats/test/dogs/
done

# Whoops, this is corrupted (in the original dataset)
rm data/dogs_and_cats/train/cats/666.jpg

rm -r -f download
