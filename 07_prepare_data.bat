@echo off

REM Download the original zip file
REM https://download.microsoft.com/download/3/E/1/3E1C3F21-ECDB-4869-8368-6DEBA77B919F/kagglecatsanddogs_3367a.zip
REM in the data directory of the PyCharm project.
REM Unzip the file and then run this batch file.

SET kaggle_dir=data\kagglecatsanddogs_3367a

if not exist %kaggle_dir% (
  echo "No Kaggle dir"
  exit /b
)

MD data\dogs_and_cats
MD data\dogs_and_cats\train
MD data\dogs_and_cats\train\cats
MD data\dogs_and_cats\train\dogs
MD data\dogs_and_cats\test
MD data\dogs_and_cats\test\cats
MD data\dogs_and_cats\test\dogs

for /l %%i in (0,1,990) do (
   MOVE %kaggle_dir%\PetImages\Cat\%%i.jpg data\dogs_and_cats\train\cats
   MOVE %kaggle_dir%\PetImages\Dog\%%i.jpg data\dogs_and_cats\train\dogs
)

for /l %%i in (1000,1,1400) do (
   MOVE %kaggle_dir%\PetImages\Cat\%%i.jpg data\dogs_and_cats\test\cats
   MOVE %kaggle_dir%\PetImages\Dog\%%i.jpg data\dogs_and_cats\test\dogs
)

REM Whoops, this is corrupted (in the original dataset)
DEL data\dogs_and_cats\train\cats\666.jpg

RD %kaggle_dir%
