
University of Pavia

Ph.D. School of Electrical and Electronics Engineering and Computer Science

# Deep Learning and TensorFlow

### A short course for PhD students

May 4 - June 21, 2018

#### Usage:
Each .py source file has an accompanying set of questions.
Open the source code file (in PyCharm) and try answering 
the corresponding questions  

## 01_graph_then_session

#### Question 01.1
Consider the two lines
```
z = tf.assign(x, tf.add(x, y))
x = tf.add(x, y)
```
What is the difference between the two?

#### Question 01.2
```
writer = tf.summary.FileWriter('graphs/off_session', tf.get_default_graph())
writer.close()
```
Visualize the graph in the above directory using Tensorboard. What is shown? 

#### Question 01.3
```
sess.run(tf.global_variables_initializer())
```
What does this line do? Is it necessary?


#### Question 01.4
```
writer = tf.summary.FileWriter('graphs/graph_then_session', sess.graph)
[...]
writer = tf.summary.FileWriter('graphs/graph_in_session')
```
What are the differences between these two lines? Why are they so?

HINT: Psst, look to this other line, just below:
```
    writer.add_graph(sess.graph)
```

#### Question 01.5
```
writer = tf.summary.FileWriter('graphs/graph_then_session', sess.graph)
[...]
writer = tf.summary.FileWriter('graphs/graph_in_session')
```
Visualize the graph in the above directories using Tensorboard.
What are the differences? 


## 02_nodes_and_names

#### Question 02.1
```
x = tf.Variable(10)
y = tf.Variable(20)
```
What happens to variables that are not given a name?

Will they have a name anyway? Which one?

#### Question 02.2
```
print('\nGlobal Variables (step 2)')
pp.pprint(tf.global_variables())
```
What happened to the definition of two variables with duplicate names?

#### Question 02.3
```
tf.reset_default_graph()
``` 
What does this line do? What if you comment it out?

#### Question 02.4
```
try:
    # This will NOT work
    x = tf.get_variable('x', initializer=10, dtype=tf.int32)
    y = tf.get_variable('y', initializer=20, dtype=tf.int32)
except Exception as e:
    print()
    print(e)

```
Are you sure you understand the exception handling mechanism?
How would you explain what happens here?

#### Question 02.5
In the above code fragment, will the line
```
    y = tf.get_variable('y', initializer=20, dtype=tf.int32)
```
be executed, ever?

#### Question 02.6
In the above code fragment, what is the reason why an exception is generated?

#### Question 02.7
```
print()
print('x == x1 : {0}'.format(x == x1))
print('y == y1 : {0}'.format(y == y1))

print('\nGlobal Variables (step 5)')
pp.pprint(tf.global_variables())
```
Please explain the printout of the above code fragment

## 03_linreg_sgd

#### Question 03.1

Explore the code that implements
```
utils.read_data_from_txt(data_filename)
```
by also analyizing file 'utils.py'. How are data prepared?

HINT: have a look to the data file 'data/birth_life_2010.txt'

#### Question 03.2
```
wGrad, bGrad = tf.gradients(loss, [w, b])
```
What does this line do? What is the expected result?

#### Question 03.3
```
wIncrement = tf.assign(w, w - learning_rate * wGrad)
bIncrement = tf.assign(b, b - learning_rate * bGrad)
``` 
Explain the above lines. Why are they in the graph?

#### Question 03.4
```
_, _, l = sess.run([wIncrement, bIncrement, loss], feed_dict={x: x_i, y: y_i})
```
Explain this line in detail. What is the expected result?

#### Question 03.5
```
writer = tf.summary.FileWriter('./graphs/linear_regression', tf.get_default_graph())
```
Use Tensorboard to explore the graph.

Find nodes 'wIncrement' and 'bIncrement'

## 04_randomization

#### Question 04.1
Is the sequence of random numbers restarted at each session?
If so, in which sessions?

#### Question 04.2
What is the meaning of 'seed' in this context?

#### Question 04.3
Why is important to 'seed' the sequence of random numbers 
in actual experiments?


## 05_logreg_mbgd_mnist

#### Question 05.1
Explore the code that implements
```
train, _, test = utils.read_mnist(mnist_folder, flatten=True)
```
by also analyizing file 'utils.py'. How are images prepared?

#### Question 05.2
Why are images _flattened_?

#### Question 05.3
In file 'utils.py', labels are converted to the _one-hot_ format.

What does this mean? Where is this done?
Why is this conversion required?

#### Question 05.4
```
img = tf.placeholder(dtype=tf.float32, shape=(batch_size, img_size))
label = tf.placeholder(dtype=tf.int32, shape=(batch_size, n_classes))
```
Explain the shape of these two placeholders

#### Question 05.5
```
logits = tf.matmul(img, W) + b
```
Is tf.matmul different from the (overloaded) operator '*'?
Could the latter be used in the same place with the same effect?

#### Question 05.6
```
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=label, logits=logits))
```
Why is tf.reduce_mean used here?


#### Question 05.7
In this training process, a TensorFlow optimizer is used:
```
# define optimizer
# using Adamn Optimizer with pre-defined learning rate to minimize loss
optimizer = tf.train.AdamOptimizer().minimize(loss)
```
What are the main differences with '03_linreg_sgd.py'? 
In particular, what do you think the optimizer does 'behind the scenes'?

#### Question 05.8
```
for j in range(n_train_batches):
    start = j * batch_size
    end = start + batch_size
    _, l = sess.run([optimizer, loss], feed_dict={img: train_img[start:end],
                                                  label: train_label[start:end]})
    total_loss += l
```
Mini-batch gradient descent (MBGD) is performed here.
Could you explain the code?

#### Question 05.9
```
batch_size = 128
```
What is the role of batch_size in MBGD?

HINT: Try different sizes, even larger ones

#### Question 05.9
With a (reasonably) small value of batch_size (e.g. 16) and n_epochs (e.g. 10)
try disabling the usage of GPU by inserting the following statement   
```
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
```
at the very beginning of the source file.

Is the execution faster or slower than with the GPU? How would you explain this result?

HINT: compare total times (see the last line). 


## 06_dcnn_mbgd_mnist

#### Question 06.1
Consider these two statements
```
    x = tf.placeholder(tf.float32, [None, 784])
    y_ = tf.placeholder(tf.float32, [None, 10])
```
what is the meaning of None in the shape of both placeholders?

#### Question 06.2
The following lines
```
    # This is typical of python Argparse (see a tutorial on internet for details)
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str,
                        default='data/mnist',
                        help='Directory for storing input data')
    FLAGS, unparsed = parser.parse_known_args()
```
are just for handling parameters in the command line.
Look for a tutorial (e.g. http://www.pythonforbeginners.com/argparse/argparse-tutorial) 
if you do not know what is the purpose of this (but do not spend too much time on it). 

#### Question 06.3
In the following statement
```
    train, _, test = utils.read_mnist(mnist_folder, flatten=True)
```
note that flatten=True, therefore images will be flattened (= transformed to vectors).

How come? How is this compatible with 2D convolution?

#### Question 06.4
Considering this function call
```
    # Build the graph for the deep net
    y_conv, keep_prob = deepnn(x)
```
analyze function deepnn and draw a block diagram describing the various layers involved.

#### Question 06.5
What is the purpose of this segment
```
    # Dropout - controls the complexity of the model, prevents co-adaptation of
    # features.
    keep_prob = tf.placeholder(tf.float32)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
```
and how does this work?

#### Question 06.6
How is dropout enabled during training and disabled during testing?

#### Question 06.7
In this example 
```
    cross_entropy = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
```
softmax is part of the loss function and not of the network proper. 
How is accuracy computed on the test set, then?

HINT: look at this part of the source code
```
    correct_predictions = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_sum(tf.cast(correct_predictions, tf.float32))
```

#### Question 06.8

Explain the program structure in the main training loop:
* When is the following printout actually executed?
```
    print('    Step {0}: training accuracy {1}'.format(step, train_accuracy / batch_size))
``` 
* When and how is accuracy computed?
* Why accuracy is computed using batches as well? (i.e. as opposed to feeding the whole test set at once)

#### Question 06.9

What is the purpose of checkpointing, in your opinion?

#### Question 06.10

Saving checkpoints. Have a look in the 'checkpoints' directory: how are file incrementally saved?

What is the content of file 'checkpoint'? (it is a text file, i.e. easy to read) 

#### Question 06.11

Try enabling the restore of the latest checkpoint
```
load_checkpoint = True
```
How does this work? Which effect does this produce?

#### Question 06.12

The following line
```
initial_epoch = int(ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1])
```
is a sort of trick to compensate for the lack of a feature in TensorFlow checkpointing. 
Could you guess what this does?


## 07_inception_v3_fine_tuning.py

#### Question 07.1

The following line is in the initial comment:
```
In summary, this is our directory structure:
```
How the image dataset is organized in the 'data/dogs_and_cats' directory?

NOTE: the overall structure is in keeping with Keras standard 
requirements for ImageDataGenerator (see below). 

#### Question 07.2
It may seem weird that input images are so small and of fixed size
```
img_width, img_height = 299, 299
```
Given that in this example we are using the Inception v3 network 
architecture, what is the reason for this?

#### Question 07.3
```
# create the base pre-trained model
base_model = InceptionV3(weights='imagenet', include_top=False)
```
Have a look to the actual definition of the Inception v3 architecture.
You will figure out why we use Keras for this example.

#### Question 07.4
```
include_top=False
```
What is the effect of this parameter option in the above statement?

#### Question 07.5
```
weights='imagenet'
```
In the above statement, the parameter option 'imagenet' makes Keras download 
and load a set of pre-trained weights.

Trace with the debugger how this is done actually and find out the url
of the pre-trained weights.  

#### Question 07.6
The following segment of code
```
# add a global spatial average pooling layer
x = base_model.output
x = GlobalAveragePooling2D()(x)
# let's add a fully-connected layer
x = Dense(1024, activation='relu')(x)
# and a logistic layer -- for dogs and cats
predictions = Dense(2, activation='softmax')(x)
```
is necessary for fine tuning. Any idea why?

HINT: recall the purpose of a network trained for ImageNet.

#### Question 07.7
```
# this is the model we will train
model = Model(inputs=base_model.input, outputs=predictions)
```
The above segment defines a Keras model (well, this is not a question)

#### Question 07.8
```
# first: train only the top layers (which were randomly initialized)
# epoch.e. freeze all convolutional InceptionV3 layers
for layer in base_model.layers:
    layer.trainable = False
```
Do you think that gradients will be computed for non-trainable layers?

#### Question 07.9
```
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
```
What does this segment do, actually?

HINT: read the Keras documentation.

#### Question 07.10
```
# prepare data augmentation configuration
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)
```
Read on Keras documentation what is a data generator and why it is used, 
as opposed to loading the whole dataset into memory 
(as it happens with all previous exercises).

#### Question 07.11
```
train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_height, img_width),
    batch_size=batch_size,
    class_mode='categorical')

```
As you can read in documentation, this statement makes Keras explore 
the above directory structure of the dataset on disk.

Make sure you understand (from Keras documentation) what kind of 
pre-processing is performed as well. 

#### Question 07.12
```
# train the model on the new data for a few epochs
history_top = model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=top_epochs,
    validation_data=test_generator,
    validation_steps=nb_test_samples // batch_size)
```
This statement makes Keras perform the actual training process.
Do you see a tf.session around? How come?

#### Question 07.13
```
# we chose to train the top 2 inception blocks, we will freeze
# the first 249 layers and unfreeze the rest:
for layer in model.layers[:249]:
    layer.trainable = False
for layer in model.layers[249:]:
    layer.trainable = True
```
In the second part of the fine-tuning training, some limitations are relaxed.
Could you explain what happens here?

Is the top level (i.e. the one added at the beginning) involved
in this second part of the training?

#### Question 07.14
```
# save trained weights
model.save_weights(os.path.join('models', new_extended_inception_weights))

# serialize modified model to JSON
model_json = model.to_json()
with open(os.path.join('models', new_extended_inception_network), 'w') as json_file:
    json_file.write(model_json)
```
This segment makes Keras save both the network architecture and 
the weights after fine-tuning. 
Have a look to the documentation to see how this works.


#### Question 07.15
```
show_history(history_fit, 'Lower layers fitting')
plt.show(block=True)
```
Explain the two diagrams obtained while running the example


## 08_inception_v3_predictions

#### Question 08.1
```
# load json and re-create model
with open(os.path.join('models', new_extended_inception_network), 'r') as json_file:
    model_json = json_file.read()

model = model_from_json(model_json)
print("Loaded network model from json file")

# load weights into new model
model.load_weights(os.path.join('models', new_extended_inception_weights))
print("Loaded network weights from hdf5 file.")
```
This segment makes Keras load both a saved network architecture and 
the weights. Have a look to the documentation to see how this works.

#### Question 08.2
```
# prepare data augmentation configuration
test_datagen = ImageDataGenerator(rescale=1. / 255)

test_generator = test_datagen.flow_from_directory(
    test_data_dir,
    shuffle=False,
    target_size=(img_height, img_width),
    batch_size=batch_size,
    class_mode='categorical')
```
Why using a generator even in this example?

#### Question 08.3

Why is 'shuffle=False' in the above segment?

Was it so also in '07_inception_v3_fine_tuning.py'?
Explain the reason for the differences, if any.

#### Question 08.4
```
# prediction can be done without compiling the model
print('Computing predictions on test set ...')
predictions = model.predict_generator(test_generator, steps=nb_test_samples / batch_size)
print('Done.')
```
Note that the model has not been compiled: have a look to Keras documentation,
this is true only for prediction (and not for training).

Why is this so?

#### Question 08.5
```
# Ground truth classes are stored in the Keras generator (after loading)
y = test_generator.classes
```
This is a built-in feature of the generator: the list of classes (i.e. labels) 
is built and kept in memory while exploring the dataset on disk.


#### Question 08.6
```
def confusion_matrix(y_valid, y_pred):
    classes = max(np.max(y_valid), np.max(y_pred)) + 1
    matrix = np.zeros((classes, classes), dtype=int)

    for i, j in zip(y_valid, y_pred):
        matrix[i, j] += 1

    return matrix
```
What is a confusion matrix?
What kind of information does it give?

HINT: look at the graphical plot produced.
